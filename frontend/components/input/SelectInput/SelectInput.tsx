import React, { VFC } from "react";
import styles from "./SelectInput.module.scss";

interface Props {
  onChange: (val: string) => void;
  options: string[];
  emptyOptionText?: string;
}

const SelectInput: VFC<Props> = ({ options, onChange, emptyOptionText }) => {
  return (
    <div className={styles.wrapper}>
      <select onChange={(e) => onChange(e.target.value)}>
        <option value="">{emptyOptionText || ""}</option>
        {options?.map((opt) => (
          <option key={opt} value={opt}>
            {opt}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectInput;
