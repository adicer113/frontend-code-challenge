import React, { FC } from "react";
import cn from "classnames";
import styles from "./Button.module.scss";

interface Props {
  className?: string;
  disabled?: boolean;
  onClick: () => void;
}

const Button: FC<Props> = ({
  className,
  disabled = false,
  onClick,
  children,
}) => {
  return (
    <button
      className={cn(styles.button, className)}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Button;
