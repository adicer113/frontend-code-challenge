import React, { VFC } from "react";
import CrossIcon from "../../../public/icons/exit.svg";
import styles from "./TextInput.module.scss";

interface Props {
  value: string;
  onChange: (val: string) => void;
  onClear?: () => void;
  placeholder?: string;
}

const TextInput: VFC<Props> = ({ value, placeholder, onChange, onClear }) => {
  return (
    <div className={styles.wrapper}>
      <input
        type="text"
        placeholder={placeholder}
        onChange={(e) => onChange(e.target.value)}
        value={value}
      />
      {onClear && <CrossIcon className={styles.clearIcon} onClick={onClear} />}
    </div>
  );
};

export default TextInput;
