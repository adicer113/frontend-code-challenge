import React, { VFC } from "react";
import cn from "classnames";
import TextInput from "../input/TextInput/TextInput";
import SelectInput from "../input/SelectInput/SelectInput";
import { EnumViewType } from "../../types";
import Button from "../input/Button/Button";
import ListIcon from "../../public/icons/list.svg";
import GridIcon from "../../public/icons/grid.svg";
import styles from "./FiltersPanel.module.scss";

interface Props {
  search: string;
  pokemonTypes: string[];
  viewType: EnumViewType;
  setShowFavorites: (val: boolean) => void;
  setSearch: (val: string) => void;
  setPokemonType: (val: string) => void;
  setViewType: (val: EnumViewType) => void;
}

const FiltersPanel: VFC<Props> = ({
  search,
  pokemonTypes,
  viewType,
  setShowFavorites,
  setSearch,
  setPokemonType,
  setViewType,
}) => {
  return (
    <div className={styles.panel}>
      <div className={styles.buttons}>
        <Button onClick={() => setShowFavorites(false)}>ALL</Button>
        <Button onClick={() => setShowFavorites(true)}>FAVORITES</Button>
      </div>
      <div className={styles.filters}>
        <TextInput
          value={search}
          placeholder="Search"
          onChange={setSearch}
          onClear={() => setSearch("")}
        />
        <SelectInput
          options={pokemonTypes}
          emptyOptionText="All"
          onChange={setPokemonType}
        />
        <div className={styles.viewSwitch}>
          <div
            className={cn(styles.viewIconContainer, {
              [styles.active]: viewType === EnumViewType.GRID,
            })}
            onClick={() => setViewType(EnumViewType.GRID)}
          >
            <GridIcon width="25" height="25" />
          </div>
          <div
            className={cn(styles.viewIconContainer, {
              [styles.active]: viewType === EnumViewType.LIST,
            })}
            onClick={() => setViewType(EnumViewType.LIST)}
          >
            <ListIcon width="25" height="25" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default FiltersPanel;
