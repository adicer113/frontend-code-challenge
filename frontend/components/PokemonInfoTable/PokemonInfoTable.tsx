import React, { VFC } from "react";
import { Pokemon } from "../../types";
import Table from "../layout/Table/Table";

interface Props {
  data: Pokemon;
  className?: string;
}

const PokemonInfoTable: VFC<Props> = ({ data, className }) => {
  const tableData = [
    ["Number", `#${data.number}`],
    ["Name", data.name],
    ["Classification", data.classification],
    ["Types", data.types?.join(", ")],
    ["HP", data.maxHP],
    ["CP", data.maxCP],
    ["Weight", `${data.weight?.minimum} - ${data.weight?.maximum}`],
    ["Height", `${data.height?.minimum} - ${data.height?.maximum}`],
    ["Evolutions", data.evolutions?.map((ev) => ev.name).join(", ") || "none"],
    ["Weaknesses", data.weaknesses?.join(", ")],
    ["Resistant", data.resistant?.join(", ")],
  ];

  return <Table data={tableData} className={className} />;
};

export default PokemonInfoTable;
