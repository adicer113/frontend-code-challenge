import React from "react";
import { useRouter } from "next/dist/client/router";
import Button from "../../input/Button/Button";
import styles from "./ErrorPage.module.scss";

const ErrorPage = () => {
  const router = useRouter();

  return (
    <div className={styles.errorPage}>
      <div className={styles.errorPageContent}>
        <span>PAGE NOT FOUND</span>
        <Button className={styles.homepageBtn} onClick={() => router.push("/")}>
          &#60; BACK TO HOMEPAGE
        </Button>
        <img src="/img/404.jpg" />
      </div>
    </div>
  );
};

export default ErrorPage;
