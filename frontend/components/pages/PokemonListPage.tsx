import React, { useState } from "react";
import { useRouter } from "next/dist/client/router";
import { PAGE_SIZE } from "../../constants";
import { EnumViewType, Pokemon } from "../../types";
import FiltersPanel from "../FiltersPanel/FiltersPanel";
import PokemonList from "../PokemonList/PokemonList";

interface FiltersType {
  page: number;
  showFavorites: boolean;
  search: string;
  pokemonType: string;
}

interface PokemonListPageProps {
  pokemonList: Pokemon[];
  total: number;
  pokemonTypes: string[];
}

const PokemonListPage = ({
  pokemonList,
  total,
  pokemonTypes,
}: PokemonListPageProps) => {
  const router = useRouter();
  const [viewType, setViewType] = useState(EnumViewType.GRID);
  const [filters, _setFilters] = useState<FiltersType>({
    page: 1,
    showFavorites: false,
    search: "",
    pokemonType: "",
  });

  const setFilters = (newFilters: Partial<FiltersType>) => {
    _setFilters({ ...filters, ...newFilters });
    router.push({
      query: {
        ...filters,
        ...newFilters,
      },
    });
  };

  const setPage = (page: number) => {
    setFilters({ page });
  };

  const setSearch = (search: string) => {
    setFilters({ page: 1, search });
  };

  const setShowFavorites = (showFavorites: boolean) => {
    setFilters({ page: 1, showFavorites });
  };

  const setPokemonType = (pokemonType: string) => {
    setFilters({ page: 1, pokemonType });
  };

  return (
    <>
      <FiltersPanel
        search={filters.search}
        pokemonTypes={pokemonTypes}
        viewType={viewType}
        setSearch={setSearch}
        setShowFavorites={(value) => setShowFavorites(value)}
        setPokemonType={setPokemonType}
        setViewType={setViewType}
      />
      <PokemonList
        data={pokemonList}
        viewType={viewType}
        paginator={{
          currentPage: filters.page,
          pageSize: PAGE_SIZE,
          total,
          cb: setPage,
        }}
      />
    </>
  );
};

export default PokemonListPage;
