import React from "react";
import { useRouter } from "next/dist/client/router";
import { Pokemon } from "../../../types";
import Button from "../../input/Button/Button";
import InfoBlock from "../../InfoBlock/InfoBlock";
import EvolutionsBlock from "../../EvolutionsBlock/EvolutionsBlock";
import styles from "./PokemonDetailPage.module.scss";

interface PokemonDetailPageProps {
  data: Pokemon;
}

const PokemonDetailPage = ({ data }: PokemonDetailPageProps) => {
  const router = useRouter();

  const goToPokemonList = () => {
    router.push("/");
  };

  return (
    <div className={styles.detail}>
      <Button onClick={goToPokemonList}>GO TO POKEMON LIST</Button>
      <>
        <div className={styles.image}>
          <img src={data.image} alt="pokemon_image" />
        </div>
        <div className={styles.info}>
          <InfoBlock data={data} />
          <EvolutionsBlock evolutions={data.evolutions} />
        </div>
      </>
    </div>
  );
};

export default PokemonDetailPage;
