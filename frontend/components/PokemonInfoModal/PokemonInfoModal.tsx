import React, { VFC } from "react";
import { Pokemon } from "../../types";
import PokemonInfoTable from "../PokemonInfoTable/PokemonInfoTable";
import styles from "./PokemonInfoModal.module.scss";

interface Props {
  data: Pokemon;
}

const PokemonInfoModal: VFC<Props> = ({ data }) => {
  return (
    <div className={styles.pokemonInfoModal}>
      <h3>Details</h3>
      <PokemonInfoTable data={data} />
    </div>
  );
};

export default PokemonInfoModal;
