import React, { VFC } from "react";
import { EnumViewType, Pokemon } from "../../types";
import GridView from "../layout/GridView/GridView";
import PokemonItem from "../PokemonItem/PokemonItem";
import styles from "./EvolutionsBlock.module.scss";

interface Props {
  evolutions: Pokemon[];
}

const EvolutionsBlock: VFC<Props> = ({ evolutions }) => {
  return (
    <div className={styles.evolutionsBlock}>
      <GridView>
        {evolutions?.map((evolution) => (
          <PokemonItem
            key={evolution.id}
            viewType={EnumViewType.GRID}
            data={evolution}
          />
        ))}
      </GridView>
    </div>
  );
};

export default EvolutionsBlock;
