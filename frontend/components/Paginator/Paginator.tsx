import React, { VFC } from "react";
import Button from "../input/Button/Button";
import styles from "./Paginator.module.scss";

export interface PaginatorData {
  currentPage: number;
  total: number;
  pageSize: number;
  cb: (page: number) => void;
}

const Paginator: VFC<PaginatorData> = ({
  currentPage,
  total,
  pageSize,
  cb,
}) => {
  const last_page = Math.ceil(total / pageSize);

  const prevPage = () => {
    cb(currentPage - 1);
  };

  const nextPage = () => {
    cb(currentPage + 1);
  };

  if (!total) {
    return null;
  }

  return (
    <div className={styles.paginator}>
      <Button onClick={prevPage} disabled={currentPage <= 1}>
        {"< PREVIOUS"}
      </Button>
      <div>
        <b>{currentPage}</b>/{last_page}
      </div>
      <Button onClick={nextPage} disabled={currentPage >= last_page}>
        {"NEXT >"}
      </Button>
    </div>
  );
};

export default Paginator;
