import React, { createRef, VFC } from "react";
import { Pokemon } from "../../types";
import SoundIcon from "../../public/icons/sound.svg";
import FavoriteButton from "../FavoriteButton/FavoriteButton";
import PokemonInfoTable from "../PokemonInfoTable/PokemonInfoTable";
import styles from "./InfoBlock.module.scss";

interface Props {
  data: Pokemon;
}

const InfoBlock: VFC<Props> = ({ data }) => {
  const audioRef = createRef<HTMLAudioElement>();

  const onSoundClick = () => {
    if (audioRef.current) {
      audioRef.current.volume = 0.2;
      audioRef.current.play();
    }
  };

  return (
    <div className={styles.infoBlock}>
      <audio ref={audioRef} src={data.sound} />
      <div className={styles.main}>
        <div className={styles.name}>{data.name}</div>
        <FavoriteButton
          id={data.id}
          name={data.name}
          isFavorite={data.isFavorite}
        />
        <SoundIcon width="25" height="25" onClick={onSoundClick} />
      </div>
      <div className={styles.info}>
        <PokemonInfoTable data={data} className={styles.infoTable} />
      </div>
    </div>
  );
};

export default InfoBlock;
