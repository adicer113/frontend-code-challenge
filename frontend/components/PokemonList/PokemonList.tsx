import React, { useState, VFC } from "react";
import { EnumViewType, Pokemon } from "../../types";
import View from "../layout/View";
import Modal from "../layout/Modal/Modal";
import Paginator, { PaginatorData } from "../Paginator/Paginator";
import PokemonInfoModal from "../PokemonInfoModal/PokemonInfoModal";
import PokemonItem from "../PokemonItem/PokemonItem";
import styles from "./PokemonList.module.scss";

interface Props {
  data: Pokemon[];
  viewType: EnumViewType;
  paginator: PaginatorData;
}

const PokemonList: VFC<Props> = ({ data, viewType, paginator }) => {
  const [detailModal, setDetailModal] = useState<Pokemon | null>(null);

  if (!data.length) {
    return (
      <div className={styles.pokemonList}>
        <div className={styles.emptyList}>List is empty</div>
      </div>
    );
  }

  return (
    <div className={styles.pokemonList}>
      <View viewType={viewType}>
        {data.map((data, idx) => (
          <PokemonItem
            key={idx}
            data={data}
            viewType={viewType}
            onDetailClick={(value) => setDetailModal(value)}
          />
        ))}
      </View>
      <Paginator
        currentPage={paginator.currentPage}
        pageSize={paginator.pageSize}
        total={paginator.total}
        cb={paginator.cb}
      />
      <Modal open={!!detailModal} onClose={() => setDetailModal(null)}>
        {detailModal && <PokemonInfoModal data={detailModal} />}
      </Modal>
    </div>
  );
};

export default PokemonList;
