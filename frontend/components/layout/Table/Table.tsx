import React, { VFC } from "react";
import cn from "classnames";
import styles from "./Table.module.scss";

interface Props {
  data: (string | number)[][];
  className?: string;
}

const Table: VFC<Props> = ({ data, className }) => {
  return (
    <table className={cn(styles.table, className)}>
      <tbody>
        {data.map((row, idx) => (
          <tr key={idx}>
            {row.map((col, idx) => (
              <td key={idx}>{col}</td>
            ))}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
