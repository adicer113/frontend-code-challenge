import React, { FC } from "react";
import { EnumViewType } from "../../types";
import GridView from "./GridView/GridView";
import ListView from "./ListView/ListView";

interface Props {
  viewType: EnumViewType;
}

const View: FC<Props> = ({ viewType, children }) => {
  return viewType === EnumViewType.GRID ? (
    <GridView>{children}</GridView>
  ) : (
    <ListView>{children}</ListView>
  );
};

export default View;
