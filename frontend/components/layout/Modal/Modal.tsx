import React, { FC } from "react";
import ReactDOM from "react-dom";
import ExitIcon from "../../../public/icons/exit.svg";
import styles from "./Modal.module.scss";

interface Props {
  open: boolean;
  onClose: () => void;
}

const Modal: FC<Props> = ({ open, onClose, children }) => {
  const modalEl =
    typeof window !== "undefined" && document.getElementById("modal");

  return modalEl
    ? ReactDOM.createPortal(
        open && (
          <div className={styles.modalWrapper} onClick={onClose}>
            <div
              className={styles.modal}
              onClick={(e) => {
                e.stopPropagation();
              }}
            >
              <ExitIcon className={styles.exitIcon} onClick={onClose} />
              {children}
            </div>
          </div>
        ),
        modalEl
      )
    : null;
};

export default Modal;
