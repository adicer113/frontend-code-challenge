import React, { FC } from "react";
import styles from "./ListView.module.scss";

export const ListView: FC = ({ children }) => {
  return <div className={styles.list}>{children}</div>;
};

export default ListView;
