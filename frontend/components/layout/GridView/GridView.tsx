import React, { FC } from "react";
import styles from "./GridView.module.scss";

const GridView: FC = ({ children }) => {
  return <div className={styles.grid}>{children}</div>;
};

export default GridView;
