import React, { VFC } from "react";
import { useMutation } from "@apollo/client";
import { toast } from "react-toastify";
import {
  ADD_POKEMON_TO_FAVORITES,
  REMOVE_POKEMON_FROM_FAVORITES,
} from "../../graphQL/mutations";
import FavoriteIcon from "../../public/icons/favorite.svg";
import styles from "./FavoriteButton.module.scss";

interface Props {
  id: string;
  name: string;
  isFavorite: boolean;
}

const FavoriteButton: VFC<Props> = ({ id, name, isFavorite }) => {
  const [addFavoritePokemon, { error: addFavError }] = useMutation(
    ADD_POKEMON_TO_FAVORITES
  );
  const [unFavoritePokemon, { error: removeFavError }] = useMutation(
    REMOVE_POKEMON_FROM_FAVORITES
  );

  const addOrRemomoveFromFavorites = () => {
    if (isFavorite) {
      unFavoritePokemon({
        variables: { id },
      });
      !removeFavError && toast(`${name} was removed from your favorites`);
    } else {
      addFavoritePokemon({
        variables: { id },
      });
      !addFavError && toast(`${name} was added to your favorites`);
    }
  };

  return (
    <div className={styles.favoriteWrapper}>
      <FavoriteIcon
        onClick={addOrRemomoveFromFavorites}
        className={isFavorite && styles.isFavorite}
      />
    </div>
  );
};

export default FavoriteButton;
