import React, { VFC } from "react";
import { useRouter } from "next/dist/client/router";
import { EnumViewType, Pokemon } from "../../types";
import FavoriteButton from "../FavoriteButton/FavoriteButton";
import MoreInfoIcon from "../../public/icons/more.svg";
import styles from "./PokemonItem.module.scss";

interface Props {
  data: Pokemon;
  viewType: EnumViewType;
  onDetailClick?: (data: Pokemon) => void;
}

const PokemonItem: VFC<Props> = ({ data, viewType, onDetailClick }) => {
  const router = useRouter();

  const onClick = () => router.push(`/${data.name}`);

  return (
    <>
      <div className={styles[`pokemonItem-${viewType}`]}>
        {onDetailClick && (
          <MoreInfoIcon
            className={styles.moreIcon}
            onClick={() => onDetailClick(data)}
          />
        )}
        <div className={styles.imageWrapper} onClick={onClick}>
          <img src={data.image} alt={`image_${data.name}`} />
        </div>
        <div className={styles.infoBlock}>
          <div>
            <div className={styles.nameText} onClick={onClick}>
              {data.name}
            </div>
            <div className={styles.typesText}>{data.types?.join(", ")}</div>
          </div>
          <FavoriteButton
            id={data.id}
            name={data.name}
            isFavorite={data.isFavorite}
          />
        </div>
      </div>
    </>
  );
};

export default PokemonItem;
