import gql from "graphql-tag";

export const ADD_POKEMON_TO_FAVORITES = gql`
  mutation addFavoritePokemon($id: ID!) {
    favoritePokemon(id: $id) {
      id
      isFavorite
    }
  }
`;

export const REMOVE_POKEMON_FROM_FAVORITES = gql`
  mutation unFavoritePokemon($id: ID!) {
    unFavoritePokemon(id: $id) {
      id
      isFavorite
    }
  }
`;
