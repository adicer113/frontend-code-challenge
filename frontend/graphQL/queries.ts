import gql from "graphql-tag";

export const GET_POKEMON_TYPES_LIST = gql`
  query {
    pokemonTypes
  }
`;

export const GET_POKEMON_LIST = gql`
  query pokemons(
    $limit: Int!
    $offset: Int!
    $search: String!
    $type: String
    $isFavorite: Boolean
  ) {
    pokemons(
      query: {
        limit: $limit
        offset: $offset
        search: $search
        filter: { type: $type, isFavorite: $isFavorite }
      }
    ) {
      count
      edges {
        id
        name
        types
        image
        isFavorite
        number
        sound
        maxHP
        maxCP
        weight {
          minimum
          maximum
        }
        height {
          minimum
          maximum
        }
        classification
        resistant
        evolutions {
          id
          name
          image
          isFavorite
        }
        attacks {
          fast {
            name
            type
            damage
          }
          special {
            name
            type
            damage
          }
        }
        weaknesses
      }
    }
  }
`;

export const GET_POKEMON_DETAIL = gql`
  query pokemonByName($name: String!) {
    pokemonByName(name: $name) {
      id
      name
      types
      image
      isFavorite
      number
      sound
      maxHP
      maxCP
      weight {
        minimum
        maximum
      }
      height {
        minimum
        maximum
      }
      classification
      resistant
      evolutions {
        id
        name
        image
        isFavorite
      }
      attacks {
        fast {
          name
          type
          damage
        }
        special {
          name
          type
          damage
        }
      }
      weaknesses
    }
  }
`;
