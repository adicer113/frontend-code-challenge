import { useQuery } from "@apollo/client";
import { GetServerSideProps } from "next";
import { useRouter } from "next/dist/client/router";
import PokemonListPage from "../components/pages/PokemonListPage";
import { PAGE_SIZE } from "../constants";
import { addApolloState, initializeApollo } from "../graphQL/client";
import { GET_POKEMON_LIST, GET_POKEMON_TYPES_LIST } from "../graphQL/queries";

const getPokemonListVariables = (
  page?: string,
  search?: string,
  pokemonType?: string,
  showFavorites?: string
) => {
  return {
    limit: PAGE_SIZE,
    offset: page ? (parseInt(page) - 1) * PAGE_SIZE : 0,
    search: search ?? "",
    type: pokemonType ?? "",
    isFavorite: showFavorites === "true",
  };
};

const Page = () => {
  const {
    query: { page, search, pokemonType, showFavorites },
  } = useRouter();

  const { data: pokemonsData, loading } = useQuery(GET_POKEMON_LIST, {
    variables: getPokemonListVariables(
      page as string,
      search as string,
      pokemonType as string,
      showFavorites as string
    ),
  });
  const pokemonList = pokemonsData?.pokemons?.edges || [];
  const total = pokemonsData?.pokemons?.count ?? 0;

  const { data: typesData } = useQuery(GET_POKEMON_TYPES_LIST);
  const pokemonTypes = typesData?.pokemonTypes;

  return (
    <PokemonListPage
      pokemonList={pokemonList}
      total={total}
      pokemonTypes={pokemonTypes}
    />
  );
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const client = initializeApollo();
  const { page, search, pokemonType, showFavorites } = context.query;

  await client.query({
    query: GET_POKEMON_LIST,
    variables: getPokemonListVariables(
      page as string,
      search as string,
      pokemonType as string,
      showFavorites as string
    ),
  });

  await client.query({
    query: GET_POKEMON_TYPES_LIST,
  });

  return addApolloState(client, {
    props: {},
  });
};

export default Page;
