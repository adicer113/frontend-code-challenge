import React from "react";
import { useQuery } from "@apollo/client";
import { GetServerSideProps } from "next";
import { useRouter } from "next/dist/client/router";
import { addApolloState, initializeApollo } from "../graphQL/client";
import { GET_POKEMON_DETAIL } from "../graphQL/queries";
import PokemonDetailPage from "../components/pages/PokemonDetailPage/PokemonDetailPage";

const Page = () => {
  const router = useRouter();
  const { data } = useQuery(GET_POKEMON_DETAIL, {
    variables: { name: router.query.name },
  });
  const pokemonDetail = data?.pokemonByName;

  return <PokemonDetailPage data={pokemonDetail} />;
};

export const getServerSideProps: GetServerSideProps = async (context) => {
  const client = initializeApollo();
  const { data } = await client.query({
    query: GET_POKEMON_DETAIL,
    variables: {
      name: context.params?.name,
    },
  });
  const pokemonDetail = data.pokemonByName;

  if (!pokemonDetail) {
    return {
      notFound: true,
    };
  }

  return addApolloState(client, {
    props: {},
  });
};

export default Page;
