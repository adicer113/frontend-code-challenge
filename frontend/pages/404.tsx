import React from "react";
import ErrorPage from "../components/pages/ErrorPage/ErrorPage";

const Custom404 = () => <ErrorPage />;

export default Custom404;
