interface PokemonDimensions {
  minimum: number;
  maximum: number;
}

interface PokemonEvolutionRequirement {
  amount: number;
  name: string;
}

interface Attack {
  name: string;
  type: string;
  damage: number;
}

interface PokemonAttack {
  fast: Attack[];
  special: Attack[];
}

export interface Pokemon {
  id: string;
  name: string;
  types: string[];
  image: string;
  isFavorite: boolean;
  number: number;
  maxHP: number;
  maxCP: number;
  weight: PokemonDimensions;
  height: PokemonDimensions;
  classification: string;
  resistant: string[];
  attacks: PokemonAttack;
  weaknesses: string[];
  fleeRate: number;
  evolutions: Pokemon[];
  evolutionRequirements: PokemonEvolutionRequirement;
  sound: string;
}

export enum EnumViewType {
  "LIST" = "LIST",
  "GRID" = "GRID",
}
